# README #


### What is this repository for? ###

 This is a movie script reader following some simple guidelines:
 It uses Java, Groovy, Spring Boot, embedded MongoDB and Spock for unit test(just the basic testing)


The endpoints for Postman could be found in: https://www.getpostman.com/collections/df3d443485489b259c7a
 
### How do I get set up? ###

 To run the test, just execute the following line (you must have Maven installer):

 mvn spring-boot:run

### Who do I talk to? ###

diogotechera@gmail.com