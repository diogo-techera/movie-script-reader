package com.movie.controller

import com.movie.MovieScriptReaderApplicationTests
import com.movie.service.ScriptService
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.setup.MockMvcBuilders

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

class ScriptControllerTest extends MovieScriptReaderApplicationTests {

    @Autowired
    ScriptController scriptController

    ScriptService scriptService

    private static final String BLANK_SCRIPT = ''''''

    private static final String SCRIPT = '''                         STAR WARS

                         Episode IV

                         A NEW HOPE

                          From the
                   JOURNAL OF THE WHILLS

                             by
                        George Lucas

                    Revised Fourth Draft
                      January 15, 1976

                       LUCASFILM LTD.



A long time ago, in a galaxy far, far, away...

A vast sea of stars serves as the backdrop for the main title.
War drums echo through the heavens as a rollup slowly crawls
into infinity.

     It is a period of civil war. Rebel spaceships,
     striking from a hidden base, have won their first
     victory against the evil Galactic Empire.

     During the battle, Rebel spies managed to steal
     secret plans to the Empire's ultimate weapon, the
     Death Star, an armored space station with enough
     power to destroy an entire planet.

     Pursued by the Empire's sinister agents, Princess
     Leia races home aboard her starship, custodian of
     the stolen plans that can save her people and
     restore freedom to the galaxy...

The awesome yellow planet of Tatooine emerges from a total
eclipse, her two moons glowing against the darkness. A tiny
silver spacecraft, a Rebel Blockade Runner firing lasers
from the back of the ship, races through space. It is pursed
by a giant Imperial Stardestroyer. Hundreds of deadly
laserbolts streak from the Imperial Stardestroyer, causing
the main solar fin of the Rebel craft to disintegrate.

INT. REBEL BLOCKADE RUNNER - MAIN PASSAGEWAY

An explosion rocks the ship as two robots, Artoo-Detoo (R2-
D2) and See-Threepio (C-3PO) struggle to make their way
through the shaking, bouncing passageway. Both robots are
old and battered. Artoo is a short, claw-armed tripod. His
face is a mass of computer lights surrounding a radar eye.
Threepio, on the other hand, is a tall, slender robot of
human proportions. He has a gleaming bronze-like metallic
surface of an Art Deco design.

Another blast shakes them as they struggle along their way.

                      THREEPIO
          Did you hear that? They've shut down
          the main reactor. We'll be destroyed
          for sure. This is madness!

Rebel troopers rush past the robots and take up positions in
the main passageway. They aim their weapons toward the door.

                      THREEPIO
          We're doomed!

The little R2 unit makes a series of electronic sounds that
only another robot could understand.

                      THREEPIO
          There'll be no escape for the Princess
          this time.

Artoo continues making beeping sounds. Tension mounts as
loud metallic latches clank and the scream of heavy equipment
are heard moving around the outside hull of the ship.

                      THREEPIO
          What's that?'''

    def setup() {
        mvc = MockMvcBuilders.webAppContextSetup(this.context).build()
        scriptService = Mock(ScriptService)
        scriptController.scriptService = scriptService
    }

    @Test
    def 'should not process empty script'() {
        when:
        def result = mvc.perform(
                post('/script').contentType(MediaType.TEXT_PLAIN).content(BLANK_SCRIPT)
        ).andReturn()
        then:
            assert result.response.status == 500
    }

    @Test
    def 'should process script'() {
        when:
        def result = mvc.perform(
                post('/script').contentType(MediaType.TEXT_PLAIN).content(SCRIPT)
        ).andReturn()
        then:
        1 * scriptService.process(_)
        assert result.response.status == 200
    }



}