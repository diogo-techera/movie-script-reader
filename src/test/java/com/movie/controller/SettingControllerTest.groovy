package com.movie.controller

import com.movie.MovieScriptReaderApplicationTests
import com.movie.domain.Setting
import com.movie.exception.NotFoundException
import com.movie.service.SettingService
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.setup.MockMvcBuilders

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

class SettingControllerTest extends MovieScriptReaderApplicationTests {

    @Autowired
    SettingController settingController

    SettingService settingService


    def setup() {
        mvc = MockMvcBuilders.webAppContextSetup(this.context).build()
        settingService = Mock(SettingService)
        settingController.settingService = settingService
    }

    @Test
    def 'should not getSettings'() {
        when:
        def result = mvc.perform(
                get('/settings')
        ).andReturn()
        then:
        1 * settingService.getSettings() >>  new RuntimeException()
        assert result.response.status == 500
        def resultAsString = result.response.contentAsString
        assert resultAsString?.contains('''"message":"Unexpected error"''')
    }
    @Test
    def 'should getSettings'() {
        when:
        def result = mvc.perform(
                get('/settings')
        ).andReturn()
        then:
        1 * settingService.getSettings() >>  [new Setting(id: 123,name: "MOS EISLEY"),new Setting(id: 456,name: "TATOOINE")]
        assert result.response.status == 200
        def resultAsString = result.response.contentAsString
        assert resultAsString?.contains('''"name":"MOS EISLEY"''')
        assert resultAsString?.contains('''"name":"TATOOINE"''')
    }

    @Test
    def 'should getSetting'() {
        when:
        def result = mvc.perform(
                get('/settings/123')
        ).andReturn()
        then:
        1 * settingService.getSetting(new BigInteger(123)) >>  new Setting(id:123,name:"MOS EISLEY")
        assert result.response.status == 200
        def resultAsString = result.response.contentAsString

        assert resultAsString?.contains('''"name":"MOS EISLEY"''')
        assert resultAsString?.contains('''"id":123''')
    }



}