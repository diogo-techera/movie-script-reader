package com.movie.controller

import com.movie.MovieScriptReaderApplicationTests
import com.movie.exception.NotFoundException
import com.movie.service.CharacterService
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import com.movie.domain.Character
import org.springframework.test.web.servlet.setup.MockMvcBuilders

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

class CharacterControllerTest extends MovieScriptReaderApplicationTests {

    @Autowired
    CharacterController characterController

    CharacterService characterService


    def setup() {
        mvc = MockMvcBuilders.webAppContextSetup(this.context).build()
        characterService = Mock(CharacterService)
        characterController.characterService = characterService
    }

    @Test
    def 'should not getCharacters'() {
        when:
        def result = mvc.perform(
                get('/characters')
        ).andReturn()
        then:
        1 * characterService.getCharacters() >>  new RuntimeException()
        assert result.response.status == 500
        def resultAsString = result.response.contentAsString
        assert resultAsString?.contains('''"message":"Unexpected error"''')
    }
    @Test
    def 'should getCharacters'() {
        when:
        def result = mvc.perform(
                get('/characters')
        ).andReturn()
        then:
        1 * characterService.getCharacters() >>  [new Character(id: 123,name: "LUKE"),new Character(id: 456,name: "LEIA")]
        assert result.response.status == 200
        def resultAsString = result.response.contentAsString
        assert resultAsString?.contains('''"name":"LUKE"''')
        assert resultAsString?.contains('''"name":"LEIA"''')
    }

    @Test
    def 'should getCharacter'() {
        when:
        def result = mvc.perform(
                get('/characters/123')
        ).andReturn()
        then:
        1 * characterService.getCharacter(new BigInteger(123)) >>  new Character(id:123,name:"LUKE")
        assert result.response.status == 200
        def resultAsString = result.response.contentAsString

        assert resultAsString?.contains('''"name":"LUKE"''')
        assert resultAsString?.contains('''"id":123''')
    }



}