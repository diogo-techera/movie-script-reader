package com.movie

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = [MovieScriptReaderApplication])
@WebAppConfiguration
@ActiveProfiles(value = ["test"])
public abstract class MovieScriptReaderApplicationTests extends Specification {

	@Autowired
	WebApplicationContext context

	MockMvc mvc

}
