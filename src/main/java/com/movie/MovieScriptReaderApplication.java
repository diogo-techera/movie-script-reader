package com.movie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@EnableAutoConfiguration
public class MovieScriptReaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieScriptReaderApplication.class, args);
	}
}
