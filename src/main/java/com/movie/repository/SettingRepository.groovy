package com.movie.repository

import com.movie.domain.Setting
import groovy.transform.CompileStatic
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
@CompileStatic
interface SettingRepository extends MongoRepository<Setting, BigInteger> {}
