package com.movie.repository

import com.movie.domain.Character
import groovy.transform.CompileStatic
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
@CompileStatic
interface CharacterRepository extends MongoRepository<Character, BigInteger> {}
