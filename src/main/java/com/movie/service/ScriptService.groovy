package com.movie.service

import com.movie.domain.Character
import com.movie.domain.Setting
import com.movie.exception.ForbiddenException
import com.movie.repository.CharacterRepository
import com.movie.repository.SettingRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.util.regex.Pattern

@Slf4j
@Service
class ScriptService {

    @Autowired
    SettingRepository settingRepository

    @Autowired
    CharacterRepository characterRepository

    Pattern SETTINGS_PATTERN = Pattern.compile("(?=INT\\.)|(?=EXT\\.)|(?=INT\\./EXT\\.)")
    Pattern SETTING_REPLACE = Pattern.compile("(INT\\.|EXT\\.|INT\\./EXT\\.)")
    String SETTING_NAME_REGEX = '( - |\n)'
    Pattern CHARACTER_REGEX = Pattern.compile("(?=\n                      [\\S]+.*)")
    Pattern CHARACTER_NAME_PATTERN = Pattern.compile('(([A-Z]| )+)\\b')
    Pattern DIALOGUE_REGEX = Pattern.compile("(?=\n          )")
    Pattern DIALOGUE_MATCHER = Pattern.compile("^\n          [\\S]+.*")

    void process(String script) {
        if(isAlreadyProcessed()){
            throw new ForbiddenException("Movie script already received")
        }
        Set<Setting> settings = [] as HashSet
        Set<Character> characters = [] as HashSet
        Map<String, List<String>> wordsPerCharacter = [:] as HashMap

        splitIntoSettings(script).each { processSetting(it,settings,characters,wordsPerCharacter)}
        addCharactersWords(characters, wordsPerCharacter)
        saveAll(characters, settings)
    }

    boolean isAlreadyProcessed() {
        settingRepository.count() > 0
    }

    private void saveAll(Set<Character> characters, Set<Setting> settings) {
        characterRepository.save(characters)
        settingRepository.save(settings)
    }

    private static void addCharactersWords(Set<Character> characters, Map<String, List<String>> wordsPerCharacter) {
        characters.each { it.addWords(wordsPerCharacter.get(it.name)) }
    }

    private String[] splitIntoSettings(String script) {
        script.split(SETTINGS_PATTERN.pattern())
    }

    def processSetting(String settingScript,Set<Setting> settings,Set<Character> characters,Map<String, List<String>> wordsPerCharacter) {
        if(isSettingBlock(settingScript)){
            Setting setting = getOrCreateSetting(settings, extractSettingName(settingScript))
            def settingCharacters = extractCharactersIntoList(settingScript).findResults{ processCharacter(it,characters, wordsPerCharacter)}.toSet()
            setting.characters.addAll(settingCharacters)
            settings.add(setting)
        }
    }

    private List<String> extractCharactersIntoList(String stringSetting) {
        stringSetting.split(CHARACTER_REGEX.pattern()).toList()
    }

    private static Setting getOrCreateSetting(Set<Setting> settings, String settingName) {
        settings.find { it.name == settingName } ?: new Setting(name: settingName)
    }

    private String extractSettingName(String stringSetting) {
        stringSetting.replaceAll(SETTING_REPLACE.pattern(), '').split(SETTING_NAME_REGEX).first().trim()
    }

    private boolean isSettingBlock(String stringSetting) {
        SETTINGS_PATTERN.matcher(stringSetting).find()
    }

    def processCharacter(String characterScript, Set<Character> characters,Map<String, List<String>> wordsPerCharacter) {
        Character character = null
        if(isCharacterBlock(characterScript)) {
            String characterName = extractCharacterName(characterScript)
            character = getOrCreateCharacter(characters, characterName)
            addCharacterWords(wordsPerCharacter, characterName, characterScript)
            characters.add(character)
        }
        character
    }

    private void addCharacterWords(Map<String, List<String>> wordsPerCharacter, String characterName, String characterScript) {
        if (wordsPerCharacter.containsKey(characterName))
            wordsPerCharacter.get(characterName).addAll(processDialogue(characterScript))
        else
            wordsPerCharacter.put(characterName, processDialogue(characterScript))
    }

    private static Character getOrCreateCharacter(Set<Character> characters, String characterName) {
        characters.find { it.name == characterName } ?: new Character(name: characterName)
    }

    private String extractCharacterName(String characterScript) {
        characterScript.find(CHARACTER_NAME_PATTERN).trim()
    }

    private boolean isCharacterBlock(String characterScript) {
        CHARACTER_REGEX.matcher(characterScript).find()
    }

    List<String> processDialogue(String dialogue) {
        List<String> words = [] as ArrayList
        splitDialogueLines(dialogue).toList().findResults { line ->
            isADialogueLine(line) ? line : null
        }.each {  words.addAll(breakIntoWords(it)) }
        words
    }

    public static List<String> breakIntoWords(String dialogueLine){
        dialogueLine.split("\\s+").collect {it.replaceAll("[^\\w]", "") }
    }

    private boolean isADialogueLine(String line) {
        DIALOGUE_MATCHER.matcher(line).find()
    }

    private String[] splitDialogueLines(String dialogue) {
        dialogue.split(DIALOGUE_REGEX.pattern())
    }
}
