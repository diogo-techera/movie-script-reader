package com.movie.service

import com.movie.domain.Character
import com.movie.exception.NotFoundException
import com.movie.repository.CharacterRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class CharacterService {
    
    @Autowired
    CharacterRepository characterRepository
    List<Character> getCharacters() {
        characterRepository.findAll()
    }

    Character getCharacter(BigInteger id) {
        def character = characterRepository.findOne(id)
        if(character)
            return character
        throw new NotFoundException("Movie character with id $id not found")

    }
}
