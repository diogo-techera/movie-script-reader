package com.movie.service

import com.movie.domain.Setting
import com.movie.exception.NotFoundException
import com.movie.repository.SettingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class SettingService {
    @Autowired
    SettingRepository settingRepository
    List<Setting> getSettings() {
        settingRepository.findAll()
    }

    Setting getSetting(BigInteger id) {
        def setting = settingRepository.findOne(id)
        if(setting)
            return setting
        throw new NotFoundException("Movie setting with id $id not found")

    }
}
