package com.movie.controller;

import com.movie.service.ScriptService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;


@RestController
@RequestMapping(value = "/script")
public class ScriptController {

    @Autowired
    private ScriptService scriptService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST,consumes = "text/plain")
    void process(@RequestBody @NotNull @NotBlank String body) {
        scriptService.process(body);
    }

    public ScriptService getScriptService() {
        return scriptService;
    }

    public void setScriptService(ScriptService scriptService) {
        this.scriptService = scriptService;
    }
}
