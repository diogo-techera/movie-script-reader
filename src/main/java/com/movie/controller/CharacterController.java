package com.movie.controller;

import com.movie.domain.Character;
import com.movie.service.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping(value = "/characters")
public class CharacterController {

    @Autowired
    private CharacterService characterService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<List<Character>> getCharacters() {
        List<Character> characters = characterService.getCharacters();
        return ResponseEntity.ok(characters);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET,value = "/{id}")
    ResponseEntity<Character> getCharacter(@PathVariable BigInteger id) {
        Character character = characterService.getCharacter(id);
        return ResponseEntity.ok(character);
    }

    public void setCharacterService(CharacterService characterService) {
        this.characterService = characterService;
    }

    public CharacterService getCharacterService() {
        return characterService;
    }
}
