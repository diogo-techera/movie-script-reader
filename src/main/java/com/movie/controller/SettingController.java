package com.movie.controller;


import com.movie.domain.Setting;
import com.movie.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping(value = "/settings")
public class SettingController {

    @Autowired
    private SettingService settingService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<List<Setting>> getSettings() {
        List<Setting> settings = settingService.getSettings();
        return ResponseEntity.ok(settings);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET,value = "/{id}")
    ResponseEntity<Setting> getSetting(@PathVariable BigInteger id) {
        Setting setting = settingService.getSetting(id);
        return ResponseEntity.ok(setting);
    }

    public void setSettingService(SettingService settingService) {
        this.settingService = settingService;
    }
}
