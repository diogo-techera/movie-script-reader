package com.movie.exception

class BaseException extends RuntimeException{

    BaseException(String message) {
        super(message)
    }
}
