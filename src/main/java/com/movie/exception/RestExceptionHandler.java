package com.movie.exception;


import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@ResponseBody
@RequestMapping(produces = "application/vnd.error")
@ControllerAdvice(annotations = RestController.class)
class RestExceptionHandler {

    @ExceptionHandler({BaseException.class})
    public ResponseEntity<?> handleApplicationException(BaseException e) {
        ResponseStatus responseStatus = AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class);
        HttpStatus status = responseStatus.value();
        Map<String, String> body = new HashMap<>();
        body.put("message",e.getMessage());
        return new ResponseEntity<>(body, status);
    }

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<?> handleGenericException(Exception e) {
        Map<String, String> body = new HashMap<>();
        body.put("message","Unexpected error");
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
