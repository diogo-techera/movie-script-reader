package com.movie.domain

import groovy.transform.EqualsAndHashCode
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed

@EqualsAndHashCode(includes = ['id','name'])
abstract class BaseEntity implements  Serializable{

    @Id
    BigInteger id

    @Indexed
    String name

}
