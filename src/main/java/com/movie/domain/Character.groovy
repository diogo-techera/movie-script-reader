package com.movie.domain

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.apache.commons.lang3.StringUtils
import org.springframework.data.mongodb.core.mapping.Document

@CompileStatic
@EqualsAndHashCode(callSuper = true)
@Document(collection = 'character')
@ToString(ignoreNulls=true,includeNames = true, includeSuper = true)
class Character extends BaseEntity{

    List<WordCount> wordCounts = [] as ArrayList

    def addWords(List<String> words) {

        words.each { word ->
            if(!StringUtils.isBlank(word) && !this.name.equals(word)){
                WordCount wordCount = new WordCount(word: word,count: 1L)
                if(wordCounts.contains(wordCount))
                    wordCounts.find{it.word == wordCount.word}.count ++
                else
                    wordCounts.add(wordCount)
            }
            this.wordCounts = this.wordCounts.size() > 10 ? this.wordCounts.subList(0,10) : this.wordCounts
            this.wordCounts.sort{-it.count}

        }

    }
}
