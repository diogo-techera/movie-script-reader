package com.movie.domain


import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = 'setting')
@EqualsAndHashCode(callSuper = true)
@ToString(ignoreNulls=true,includeNames = true, includeSuper = true)
class Setting extends BaseEntity{

    Set<Character> characters = [] as HashSet

}
