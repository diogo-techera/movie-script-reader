package com.movie.domain

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString


@EqualsAndHashCode(includes = ['word'])
@ToString(ignoreNulls=true,includeNames = true)
class WordCount {
    String word
    Long count = 0L
}
